# Pitchfork Score

## Datebase Structure
_Note: I'm using `back_populates` rather than `backref` because I want to visually track and remember what is linked with what._

These are the models that basically breakdown the tables that will be used in the db. These are the relationships:

Artists:
- Attributes
    * Artists have a name
    * Artists have a unique id on pitchfork

- Relationships
    * Artists can have multiple albums

Authors:
- Attributes
    * Authors have a name
    * Authors have a unique id on pitchfork
    * Authors have a pitchfork URL
    * Authors have a title at pitchfork

- Relationships
    * Authors can have many reviews

Album:
- Attributes
    * An Album has a name
    * An Album has a pitchfork ID
    * An Album has a pitchfork URL
    * An Album has a decimal score
    * An Album has a blurb of text 
    * An Album has a publishing date

- Relationships
    * An Album can only have one Author
    * An Album can only have multiple Genre
    * An Album can have multiple artists (Various artists do not have a pitchfork ID)

Genre:
- Attributes
    * A Genre has a name
- Relationships
    * An album can only have one genre (back reference)

