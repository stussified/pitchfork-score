from flask_restplus import Api
from flask import Blueprint

from main.controller.albums_controller import api as albums_ns

blueprint = Blueprint(
    'api',
    __name__,
    static_folder='./static'
    )

api = Api(
    blueprint,
    title="Pitchfork API",
    version="1.0",
    description="This is a api that aggregates Pitchfork reviews"
          )

api.add_namespace(albums_ns, path='/albums')