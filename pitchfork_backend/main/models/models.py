# use db that's instantiated by the factor application pattern?
from main.api import db, create_app

# make an association table so you can get all the artists that are related to an album and all the albums related to an artist (many to many)
artist_album_association = db.Table(
    'artist_album',
    db.Column('artists_id', db.Integer, db.ForeignKey('artists.id')),
    db.Column('albums_id', db.Integer, db.ForeignKey("albums.id"))
)

album_genre_association = db.Table(
    'album_genre',
    db.Column('albums_id', db.Integer, db.ForeignKey("albums.id")),
    db.Column('genres_id', db.Integer, db.ForeignKey("genres.id"))
)

arthor_album_association = db.Table(
    'author_album',
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id')),
    db.Column('album_id', db.Integer, db.ForeignKey('albums.id')))

class Artists(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=False)
    pitchfork_id = db.Column(db.String(100), unique=True)
    albums = db.relationship("Albums", secondary=artist_album_association, back_populates="artists", lazy='dynamic')

class Authors(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pitchfork_id = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(100), nullable=False)
    title = db.Column(db.String(100))
    url = db.Column(db.String(200))
    albums = db.relationship('Albums', secondary=arthor_album_association, back_populates='authors', lazy='dynamic')

class Genres(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)

    # an album can have multiple genres
    albums = db.relationship("Albums", secondary=album_genre_association, back_populates="genres", lazy='dynamic')

class Albums(db.Model):
    # it currently does not seem possible for a review to have multiple authors
    id = db.Column(db.Integer, primary_key=True)
    album_title = db.Column(db.String(500), nullable=False)
    pitchfork_id = db.Column(db.String(500), nullable=False, unique=True)
    review_url = db.Column(db.String, nullable=False)
    score = db.Column(db.DECIMAL, nullable=False)
    description = db.Column(db.String(500))
    pub_date = db.Column(db.Date)

    # an album can have multiple artists and an artist can have multiple albums
    # therefore you need a secondary association
    artists = db.relationship("Artists", secondary=artist_album_association, back_populates="albums", lazy='dynamic')

    # an album can also have multiple genres
    genres = db.relationship("Genres", secondary=album_genre_association, back_populates="albums", lazy='dynamic')

    # an album has a singular author, but writing it as a list
    authors = db.relationship('Authors', secondary=arthor_album_association, back_populates="albums", lazy='dynamic')

def main():
    pass

if __name__ == "__main__":
    main()