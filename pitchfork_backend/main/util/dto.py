"""
dto stands for Data Transfer Object - this is basically the models that
Flask-restful will use when returning data to the API.  Flask-restful
will also allow us to use this API tool called swagger which looks dope.
"""

from flask_restplus import Namespace, fields

class AlbumsDto:
    api = Namespace('albums', description='album retrieval endpoint')
    album = api.model(
        'albums', {
            "album_title": fields.String(required=True, description="Name of the album"),
            "pitchfork_id": fields.String(required=True, description="identifier of the album on Pitchfork.com"),
            "review_url": fields.String(required=True, description="URL of there review on Pitchfork.com"),
            "score": fields.Float(required=True, description="Score of the album on Pitchfork.com"),
            "pub_date": fields.Date(required=True, description="Date of publishing"),
            "artists": fields.List(fields.String, required=True, description="List of artists of the album"),
            "genres": fields.List(fields.String, required=True, description="List of genres of the album"),
            "authors": fields.List(fields.String, required=True, description="List of authors (usually only 1)"),
            "description": fields.String(required=True, description="Review blurb from Pitchfork.com")
        }
    )