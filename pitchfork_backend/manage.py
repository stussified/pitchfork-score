import os
from main import blueprint
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from main.api import create_app, db

# this is the model migration for flask_sqlalchemy and flask_migrate
from main.models.models import Artists, Authors, Genres, Albums


app = create_app(os.getenv('BOILERPLATE_ENV') or "dev")
app.register_blueprint(blueprint)
app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run()

if __name__ == "__main__":
    manager.run()