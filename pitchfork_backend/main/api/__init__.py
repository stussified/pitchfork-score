from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from .config import config_by_name

db = SQLAlchemy()

def create_app(config_name):
    app = Flask(__name__, static_folder=None)
    # CORS lets our front end interact with flask without CSRF issues or whatever
    CORS(app, resources={r'/*': {'origins': '*'}})
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    return app