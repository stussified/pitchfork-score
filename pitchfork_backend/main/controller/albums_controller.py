from flask import request
from flask_restplus import Resource, reqparse
from main.service.album_service import get_albums, search_query
from main.util.dto import AlbumsDto

api = AlbumsDto.api
albums = AlbumsDto.album

@api.route('/')
@api.route('/<int:page>')
class AlbumList(Resource):
    @api.doc('list_of_albums')
    @api.marshal_list_with(albums, envelope="data")

    def get(self, page=1):
        return get_albums(page)  # this doesn't exist yet

@api.route('/search')
class SearchResults(Resource):
    @api.doc('search_artists_albums', params={"term": "Search term for album or artist name"})
    @api.marshal_list_with(albums, envelope="data")

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("term", type=str, help="Search artists name and album title. Returns max 20 results")
        args = parser.parse_args()
        return search_query(args['term'])
