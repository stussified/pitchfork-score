import os
from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse
from flask_sqlalchemy import SQLAlchemy
from pitchfork_backend import Artists, Albums, Authors
from flask_cors import CORS

db_path = os.path.join(os.path.abspath(os.path.dirname(__name__)), "pitchfork.db")
db_uri = "sqlite:///{}".format(db_path)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

cors = CORS(app, resources={r"/*": {"origins": "*"}})
db = SQLAlchemy(app)
api = Api(app)


class Artist_Endpoint(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name", type=str, help="Album title")
        parser.add_argument("limit", type=int, help="Limit of search results")
        args = parser.parse_args()

        name = args.get("name")
        limit = args.get("limit") or 10

        data = db.session.query(Artists).filter(Artists.name.like("%{}%".format(name))).limit(limit).all()
        return jsonify([{"name": i.name,
                 "pitchfork_id": i.pitchfork_id} for i in data])


class Album_Endpoint(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("title", type=str, help="Album title")
        parser.add_argument("limit", type=int, help="Limit of search results")
        args = parser.parse_args()
        album_title = args.get("title")
        limit = args.get("limit") or 10
        if album_title:
            data = db.session.query(Albums).\
                filter(Albums.album_title.like("%{}%".format(album_title)))\
                .limit(limit).all()
        else:
            data = db.session.query(Albums).limit(limit).all()

        return jsonify([{
            "album_title": i.album_title,
            "pitchfork_id": i.pitchfork_id,
            "artists": [a.name for a in i.artists],
            "score": str(round(i.score, 2)),
            "author": i.author.name,
            "description": i.description
        } for i in data])

class Author_Endpoint(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name", type=str, help="Name of the Author")
        parser.add_argument("limit", type=int, help="Limit of search results")
        args = parser.parse_args()
        author = args.get("name")

        data = db.session.query(Authors).filter(Authors.name.like("%{}%".format(author))).first()
        return jsonify([
            {"title": i.album_title,
             "artists": [x.name for x in i.artists],
             "score": str(round(i.score, 2)),
             "description": i.description
             }
            for i in data.albums])



api.add_resource(Artist_Endpoint, '/artist')
api.add_resource(Album_Endpoint, '/album')
api.add_resource(Author_Endpoint, '/author')
#
# @app.route('/')
# def index():
#
#     data = db.session.query(Artists).limit(10).all()
#     return ", ".join([i.name for i in data])

if __name__ == "__main__":
    app.run(debug=True)