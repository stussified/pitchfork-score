"""
These tests basically ensure that the flask configurations for production, testing and development have the right config values
"""

import os
import pytest

from pitchfork_backend.manage import app, create_app
from pitchfork_backend.api.config import basedir

@pytest.fixture
def development_app():
    app = create_app()
    return app

def test_app_is_development():
    app.config.from_object('pitchfork_backend.api.config.DevelopmentConfig')
    assert app.config['DEBUG'] == True
    assert app.config["SQLALCHEMY_DATABASE_URI"] == 'sqlite:///{}'.format(os.path.join(basedir, 'pitchfork_test.db'))
    assert app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] == False

def test_app_is_testing():
    app.config.from_object('pitchfork_backend.api.config.TestingConfig')
    assert app.config['DEBUG'] == True
    assert app.config['TESTING'] == True
    assert app.config["SQLALCHEMY_DATABASE_URI"] == 'sqlite:///{}'.format(os.path.join(basedir, 'pitchfork_test.db'))
    assert app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] == False
    assert app.config["PRESERVE_CONTEXT_ON_EXCEPTION"] == False

def test_app_is_production():
    app.config.from_object('pitchfork_backend.api.config.ProductionConfig')
    assert app.config['DEBUG'] == False
    assert app.config["SQLALCHEMY_DATABASE_URI"] == 'sqlite:///{}'.format(os.path.join(basedir, 'pitchfork.db'))


if __name__ == "__main__":
    pass