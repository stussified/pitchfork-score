from main.api import create_app
from main.models.models import db, Albums, Artists

def album_obj_to_dict(sqlalchemy_obj):
    return [{
            "artists": [x.name for x in i.artists],
            "album_title": i.album_title,
            "score": float(i.score),
            "description": i.description,
            "pub_date": i.pub_date,
            "authors": [x.name for x in i.authors],
            "genres": [x.name for x in i.genres],
            "pitchfork_id": i.pitchfork_id,
            "review_url": i.review_url
        } for i in sqlalchemy_obj]

def get_albums(page=1):
    per_page = 10
    query = Albums.query.paginate(page, per_page, error_out=False)
    return album_obj_to_dict(query.items)

def search_query(search_term):
    # add wildcards to the search term
    search_term = "%{}%".format(search_term)
    query = album_obj_to_dict(Albums.query.join(Artists.albums).filter(Albums.album_title.ilike(search_term)|Artists.name.ilike(search_term)).limit(20).all())
    return query

if __name__ == "__main__":
    app = create_app('dev')
    app.app_context().push()
    test = get_albums()
    query = db.session.query(Albums).limit(10).all()
