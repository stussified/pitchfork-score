"""
This file is going to be all the functions that are used in order to scrape pitchfork.
"""

import requests
import json
from collections import namedtuple
import datetime

# this is the stuff to write data or images to the db.
from main.api import db, create_app
from main.models.models import Artists, Authors, Genres, Albums
import os



static_image_folder = os.path.abspath("../static/images/album_covers")
session = requests.Session()
PITCHFORK_API_URL = "https://pitchfork.com/api/v2/search"

def search(size=200, start=0, end=0):
    """

    :param size: how many reviews to get - int
    :param start: starting index of reviews to get from API
    :param end: ending index of reviews to get from API
    :return: json object from pitchfork containing review data
    """
    search = session.get(PITCHFORK_API_URL,
        params={
            "types": "reviews",
            "hierarchy": "sections/reviews/albums,channels/reviews/albums", # idk what this means BUT DO NOT PUT A SPACE BY THE COMMA
            "sort": "publishdate asc,position asc",  #  use asc so you don't get a weird ass id incrementation jump from latest album to oldest
            "size": size,  # 200 is the max
            "start": start,
            "end": end
        })

    return search.json()

def get_range_scores():
    return search(size=1).get("count")

def get_latest_scores():
    album_list = []
    pitchfork_search = search()
    for album in pitchfork_search.get("results").get("list"):
        album_list.append(album)

    return json.dumps(album_list)

def review_parser(album_json):
    """
    this function takes in a singular pitchfork review json object and then returns the album data that the db wants
    :param json_object: string from pitchfork api
    :return: dictionary
    """
    review_data = {
            "albums": {
            "album_title": album_json.get("title"),
            "pub_date": datetime.datetime.strptime(album_json.get("pubDate").split("T")[0], "%Y-%m-%d").date(),  # fuck date time parsing
            "pitchfork_id": album_json.get("id"),
            "review_url": "{}{}".format("http://pitchfork.com", album_json.get("url", "/")),
            "score": album_json.get("tombstone").get("albums")[0].get("rating").get("rating"),
            "description": album_json.get("socialDescription")
        },
        "authors": [{
            "name": author.get("name"),
            "pitchfork_id": author.get("id"),
            "title": author.get("title"),
            "url": "{}{}".format("http://pitchfork.com", author.get("url"))
        } for author in album_json.get("authors")],
        "artists": [{
            "name": artist.get("display_name"),
            "pitchfork_id": artist.get("id"),
        } if album_json.get("artists") else {"name": "Various Artists"} for artist in album_json.get("artists")],
        "genres": [{
            "name": genre.get("display_name")
        } for genre in album_json.get("genres")]}

    return review_data

def populate_db(pitchfork_reviews):
    for raw_album in pitchfork_reviews.get("results").get("list"):
        # skip janky json objects that don't have info or albums without score
        if not raw_album.get("tombstone", {}).get("albums") or not raw_album.get("tombstone").get("albums")[0].get("rating").get("rating"):
            continue

        album = review_parser(raw_album)

        # skip duplicate albums
        if db.session.query(Albums).filter(Albums.pitchfork_id == album.get("albums").get("pitchfork_id")).first():
            continue

        # create the new album object
        album_obj = Albums(**album.get("albums"))

        # if artist doesn't exist, add to album object else use existing object
        for artist in album.get("artists"):
            existing_artist = db.session.query(Artists).filter(Artists.pitchfork_id == artist.get("pitchfork_id")).first()
            album_obj.artists.append(existing_artist or Artists(**artist))

        # if authors doesn't exist, add to album object else use existing object
        for author in album.get("authors"):
            existing_author = db.session.query(Authors).filter(Authors.pitchfork_id == author.get("pitchfork_id")).first()
            album_obj.authors.append(existing_author or Authors(**author))

        # if genre doesn't exist, add to album object else use existing object
        for genre in album.get("genres"):
            existing_genre = db.session.query(Genres).filter(Genres.name == genre.get("name")).first()
            album_obj.genres.append(existing_genre or Genres(**genre))

        # download cover images
        download_image(raw_album)

        db.session.add(album_obj)
        print("writing {} by {}".format(album.get("albums").get("album_title"), ", ".join([i.get("name") for i in album.get("artists")])))
        db.session.commit()

def get_all_scores():
    "this should probably be in a loop because you want it to write it each time it downloads stuff, not save it til the end"
    total_review_count = get_range_scores()
    paginated_range = namedtuple("paginated_range", "start end")
    count_list = [paginated_range(start=i, end=i + 200) if i + 200 <= total_review_count else paginated_range(start=i, end=total_review_count) for i in range(0, total_review_count, 200)]

    for interval in count_list:
        print("running between {} and {}".format(interval.start, interval.end))
        pitchfork_search = search(start=interval.start, end=interval.end)
        populate = populate_db(pitchfork_search)

def download_image(pitchfork_album_obj):
    """
    This function is going to download all the album images from pitchfork.
    the structure is going to be {pitchfork_id}_large.jpg or {pitchfork_id}_small.jpg
    The keys will be "standard" and "homepageLarge" respectively.  They'll
    be stored in the /static folder.  This will be in a loop of some type.
    """
    album_id = pitchfork_album_obj.get("id")
    filename_template = os.path.join(static_image_folder, album_id)
    image_dict = pitchfork_album_obj.get("tombstone", {}).get("albums", [])[0].get("album", {}).get("photos", {}).get("tout", {}).get("sizes", {})

    large_image = image_writer(image_dict.get("standard"), "{}_large.jpg".format(filename_template))
    small_image = image_writer(image_dict.get("homepageLarge"), "{}_small.jpg".format(filename_template))

def image_writer(image_url, abs_filename):
    image_download = session.get(image_url, stream=True)
    with open(abs_filename, 'wb') as writer:
        for chunk in image_download:
            writer.write(chunk)

def main():
    app = create_app('dev')
    app.app_context().push()
    get_all_scores()

if __name__ == "__main__":
    main()